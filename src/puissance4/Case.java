package puissance4;

import javax.swing.*;
import java.awt.*;

public class Case extends JPanel {

  int val;
  int ligne;
  int col;
  Jeu jeu;
  Color bgCol;
  boolean preview;

  public Case(int ligne, int col, int a, Jeu j) {
    super();
    this.val = a;
    this.ligne = ligne;
    this.col = col;
    this.jeu = j;
    this.bgCol = Color.blue;
    this.preview = true;
  }

  public void modifierVal(int a) {
    this.val = a;
    this.bgCol = Color.blue; // Pour bien confirmer le fond blanc de la case
    repaint();
  }

  public void modifierBg(Color c) {
    this.bgCol = c;
    repaint();
  }

  public void paintComponent(Graphics comp) {
    Graphics2D comp2D = (Graphics2D) comp;
    comp2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON); // Pour l'antialias

    comp2D.setColor(bgCol);
    comp2D.fillRect(0, 0, getWidth(), getHeight());
    //comp2D.setColor(Color.black);
    //comp2D.drawRect(0, 0, getWidth(), getHeight());
    comp2D.setColor(Color.white);
    comp2D.fillOval(3, 3, getWidth() - 4, getHeight() - 4);

    if (val != 0) {
      if (val == 1) {
        comp2D.setColor(Color.red);
      } else {
        comp2D.setColor(Color.yellow);
      }
      comp2D.fillOval(3, 3, getWidth() - 4, getHeight() - 4);
    }
  }

}
